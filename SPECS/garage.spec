Name:           garage
Version:        0.9.3
Release:        1%{?dist}
Summary:        S3-compatible object store for small self-hosted geo-distributed deployments

License:        AGPLv3
URL:            https://garagehq.deuxfleurs.fr/
Source0:        https://git.deuxfleurs.fr/Deuxfleurs/%{name}/archive/v%{version}.tar.gz

BuildRequires:  cargo clang

AutoReq:        no
AutoReqProv:    no

%description
An open-source distributed storage service you can self-host to fullfill many needs

%prep
%setup -q -n %{name}

%build
cargo build --release

%install
mkdir -p %{buildroot}%{_bindir}

install target/release/garage %{buildroot}%{_bindir}


%files
%{_bindir}/garage
%license LICENSE

%changelog
* Sun Mar 10 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.9.3-1
- Update to 0.9.3

* Tue Mar 21 2023 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.8.2-1
- Update to 0.8.2

* Fri Dec 23 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.8.0-1
- Update to 0.8.0

* Sat Sep 17 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.7.3-1
- Update to 0.7.3

* Sat May 14 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.7.1-1
- Update to new version

* Thu Mar 3 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.6.1-1
- Initial rpm
